FROM python:3.10-slim-buster

# Don't write .pyc files
ENV PYTHONDONTWRITEBYTECODE 1
# Don't buffer stdout and stderr output
ENV PYTHONUNBUFFERED 1

COPY requirements/requirements.txt /tmp/requirements.txt

RUN pip install --upgrade pip pip-tools && \
    pip install -r /tmp/requirements.txt

COPY . /src/

WORKDIR /src/

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
