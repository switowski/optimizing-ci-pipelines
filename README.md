# A dummy Django project for my CI talk

This is a dummy Django project created from <https://github.com/jefftriplett/django-startproject> with a simple TODO application.

Its only purpose is to be used as an example app for my "Optimizing Your CI Pipelines" talk.

If you're reading this after the summer of 2023, you can probably find my talk on Youtube. If you're reading it earlier, come to [PyCon Italia](https://pycon.it/en) or [EuroPython](https://ep2023.europython.eu/) to see it live 😉.

This is **NOT production-ready** (e.g., DB is configured with "POSTGRES_HOST_AUTH_METHOD=trust" setting to allow anyone to access).

## Structure of this project

The most important parts of this project are the following:

* `todo` folder contains a simple TODO app. It's ugly, but it interacts with the database, and we have some tests that check if adding or completing tasks works correctly. So that's enough to simulate a small application.
* `requirements.in` (and in turn `requirements.txt` - run `make deps` to update `requirements.txt` based on the content of `requirements.in` file) contains packages that are required to run the this project, but also a bunch of dummy packages. This makes the Docker build take a bit longer, so we have some room for improvement in my talk.
* `tests` folder contains some dummy tests that sleep for a few seconds. Again, the main purpose of this is to make the whole test suite slower. By default running all tests with pytest takes around 1.5 minute in Gitlab CI. We will be improving that in my talk.
* The build process uses Docker and docker compose. Docker compose will set up two services: a PostgreSQL DB and a web container. Web image is built from a pretty standard Dockerfile - we specify Python 3.10 base image, install some dependencies, copy the code and finally start the server.

## Usage

```shell
docker compose build
docker compose up
```

See the Makefile for additional commands.
