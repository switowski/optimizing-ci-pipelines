shell:
	docker compose run --rm web python manage.py shell_plus --ipython
deps:
	docker-compose run --rm web pip-compile requirements/requirements.in
dev:
	docker-compose up
test:
	docker compose run --rm web pytest -n auto
