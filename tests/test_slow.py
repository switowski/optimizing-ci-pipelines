# Dummy tests that perform a bunch of useless operations


def test_slow_math_one():
    num_iterations = 50_000_000
    result = 0

    for i in range(num_iterations):
        result = ((i * 2) / 3) ** 2
        result *= 4
        result = abs(result - 10)

    assert result > 1


def test_slow_math_two():
    num_iterations = 75_000_000
    result = 0

    for i in range(num_iterations):
        result = ((i * 2) / 3) ** 2
        result *= 4
        result = abs(result - 10)

    assert result > 1


def test_slow_math_three():
    num_iterations = 100_000_000
    result = 0

    for i in range(num_iterations):
        result = ((i * 2) / 3) ** 2
        result *= 4
        result = abs(result - 10)

    assert result > 1
